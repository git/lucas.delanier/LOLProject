﻿using DTO;
using EntityFrameworkLOL.DBContexts;
using EntityFrameworkLOL.Entities;
using Model;

namespace APILOL.Mapper
{
    public static class ChampionMapper
    {
        public static ChampionDTO ToDto(this Champion champion)
        {
            return new ChampionDTO()
            {
                Name = champion.Name,
                Bio = champion.Bio,
                Class = champion.Class,
                Icon = champion.Icon,
                Image = champion.Image,
                Skills = champion.Skills.Select(skill => skill.ToDto()),
            };
        }

        public static Champion ToModel(this ChampionDTO champion)
        {
            return new Champion(champion.Name, champion.Class, champion.Icon, champion.Image.ToString(), champion.Bio);
        }

        public static Champion ToModel(this ChampionEntity entity)
        {
            var champion = new Champion(entity.Name, entity.Class, entity.Icon, "", entity.Bio);
            if (entity.Skills != null) foreach (var s in entity.Skills) { champion.AddSkill(s.ToModel()); }
            if (entity.Characteristics != null) foreach (var c in entity.Characteristics) { champion.AddCharacteristics(c.ToModel()); }
            return champion;
        }

        public static ChampionEntity ToEntity(this Champion item, SQLiteContext context)
        {
            ChampionEntity? championEntity = context.Champion.Find(item.Name);
            if (championEntity == null)
            {
                championEntity = new()
                {
                    Name = item.Name,
                    Bio = item.Bio,
                    Icon = item.Icon,
                    Class = item.Class,
                    Image = new() { Base64 = item.Image.Base64 },
                };
                championEntity.Skills = item.Skills.Select(x => x.ToEntity(championEntity, context)).ToList();
                championEntity.Characteristics = item.Characteristics.Select(x => x.ToEntity(championEntity, context)).ToList();
            }
            return championEntity;
        }
    }
}