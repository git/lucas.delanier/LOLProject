﻿using APILOL.Controllers.Request;
using APILOL.Mapper;
using DTO;
using Microsoft.AspNetCore.Mvc;
using Model;
using StubLib;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace APILOL.Controllers.v1
{
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1.0")]
    public class RuneController : ControllerBase
    {
        IRunesManager dataManager;

        private readonly ILogger<RuneController> _logger;

        public RuneController(IDataManager dataManager, ILogger<RuneController> logger)
        {
            this.dataManager = dataManager.RunesMgr;
            this._logger = logger;

        }



        // GET: api/<RuneController>
        [MapToApiVersion("1.0")]
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] PageRequest request)
        {
            _logger.LogInformation("API call - [GET] - RUNE");
            try
            {

                var total = await dataManager.GetNbItems();
                var runes = await dataManager.GetItems(request.Offset, request.Limit, request.OrderingPropertyName, request.IsDesc);
                IEnumerable<RuneDTO> items = runes.Select(c => c.ToDto());
                if (items.Count() == 0)
                {
                    _logger.LogInformation("No rune found.");
                    return NotFound("No rune found.");
                }
                return Ok(new { data = items, count = await dataManager.GetNbItems(), offset = request.Offset, limit = request.Limit });
            }
            catch (Exception error)
            {
                return BadRequest(error.Message);
            }

        }

        // GET api/<RuneController>/5
        [MapToApiVersion("1.0")]
        [HttpGet("{name}")]
        public async Task<IActionResult> Get([FromQuery] PageRequest request, string name)
        {
            _logger.LogInformation("API call - [GET / NAME] - RUNE");
            try
            {
                if (dataManager.GetNbItemsByName(name) != null)
                {
                    var runes = await dataManager.GetItemsByName(name, request.Offset, request.Limit, request.OrderingPropertyName, request.IsDesc);
                    IEnumerable<RuneDTO> items = runes.Select(c => c.ToDto());
                    if (items.Count() == 0)
                    {
                        _logger.LogInformation("No rune found.");
                        return BadRequest("No rune found.");
                    }

                    return Ok(items);
                }
                return NotFound("No rune matching with this name.");
            }
            catch (Exception error)
            {
                return BadRequest(error.Message);
            }
        }

        // POST api/<RuneController>
        [MapToApiVersion("1.0")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] RuneDTO runeDTO)
        {
            _logger.LogInformation("API call - [POST] - RUNE");

            try
            {
                if (await dataManager.GetNbItemsByName(runeDTO.Name) == 0)
                {
                    await dataManager.AddItem(runeDTO.ToModel());
                    return CreatedAtAction(nameof(Get), runeDTO);
                }
                _logger.LogInformation("A rune already exist with this Name. ( Unique Name )");
                return BadRequest("A rune already exist with this Name. ( Unique Name )");
            }
            catch (Exception error)
            {
                _logger.LogInformation("Error in the request");
                return BadRequest(error.Message);
            }
        }

        // PUT api/<Rune>/5
        [MapToApiVersion("1.0")]
        [HttpPut("{name}")]
        public async Task<IActionResult> PutAsync(string name, [FromBody] RuneDTO runeDTO)
        {
            _logger.LogInformation("API call - [PUT / NAME] - RUNE");
            try
            {
                var rune = await dataManager
                .GetItemsByName(name, 0, await dataManager.GetNbItems());
                Console.WriteLine(rune.First().Name);
                var rune2 = await dataManager
                .GetItemsByName(runeDTO.Name, 0, await dataManager.GetNbItems());
                if (rune != null)
                {
                    if (rune2.Count() == 0)
                    {
                        await dataManager.UpdateItem(rune.First(), runeDTO.ToModel());
                        return Ok();

                    }
                    _logger.LogInformation("rune already exist with this unique name.");
                    return BadRequest("rune already exist with this unique name.");

                }
                else
                {
                    _logger.LogInformation("rune not found.");
                    return NotFound("rune not found.");
                }
                await dataManager.UpdateItem(rune.First(), runeDTO.ToModel());
                return Ok();

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        // DELETE api/<RuneController>/5
        [MapToApiVersion("1.0")]
        [HttpDelete("{name}")]
        public async Task<IActionResult> Delete(string name)
        {
            _logger.LogInformation("API call - [DELETE / NAME] - RUNE");
            try
            {
                var rune = await (dataManager.GetItemsByName(name, 0, await dataManager.GetNbItems()));

                if (rune.Count() != 0)
                {
                    var runeDto = rune.First().ToDto();
                    await dataManager.DeleteItem(rune.First());
                    return Ok(runeDto);
                }
                else
                {
                    _logger.LogInformation("No matching rune with this name");
                    return NotFound("No matching rune with this name");
                }

            }
            catch (Exception error)
            {
                return BadRequest(error);
            }
        }
    }
}
