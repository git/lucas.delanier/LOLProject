﻿using EntityFrameworkLOL.Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Model;

namespace EntityFrameworkLOL.DBContexts
{
    public class SQLiteContext : DbContext
    {
        public DbSet<ImageEntity> Image { get; set; }
        public DbSet<SkillEntity> Skill { get; set; }
        public DbSet<SkinEntity> Skin { get; set; }
        public DbSet<RuneEntity> Rune { get; set; }
        public DbSet<RunePageEntity> RunePage { get; set; }
        public DbSet<CharacteristicEntity> Characteristic { get; set; }
        public DbSet<ChampionEntity> Champion { get; set; }

        public SQLiteContext() { }

        public SQLiteContext(DbContextOptions<SQLiteContext> options)
           : base(options)
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            //var connection = new SqliteConnection("DataSource=:memory:");
            var connection = new SqliteConnection("Data Source=DBLOL.db");
            connection.Open();
            options.UseSqlite(connection);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ImageEntity>().Property(i => i.Base64).ValueGeneratedOnAdd();
            modelBuilder.Entity<CharacteristicEntity>().HasKey(c => new { c.Name });
            modelBuilder.Entity<RunePageEntity>().Property(rp => rp.Name).ValueGeneratedOnAdd();
            modelBuilder.Entity<RunePageEntity>()
                        .HasMany(rp => rp.Runes);
            modelBuilder.Entity<ChampionEntity>()
                        .HasMany(c => c.RunePages)
                        .WithMany(rp => rp.Champions);
            modelBuilder.Entity<ImageEntity>().HasData(new List<ImageEntity>()
            {
                new()
                {
                    Base64 = "default"
                }
            });
            modelBuilder.Entity<ChampionEntity>().HasData(new List<ChampionEntity>() {
                new()
                {
                    Name = "WinKer",
                    Bio = "Best front-end designer",
                    Class = ChampionClass.Mage,
                    Icon = "",
                },
                new()
                {
                    Name = "Jonquille",
                    Bio = "Daffodil",
                    Class = ChampionClass.Support,
                    Icon = "",
                }
            });
            modelBuilder.Entity<CharacteristicEntity>().HasData(new List<CharacteristicEntity>() {
                new()
                {
                    Name = "Front-end",
                    Value = 100,
                },
                new()
                {
                    Name = "Back-end",
                    Value = 100,
                }
            });
            modelBuilder.Entity<SkinEntity>().HasData(new List<SkinEntity>() {
                new SkinEntity
                {
                    Name = "Darker WinKer",
                    Description = "Be invisible in the darkness but never alone",
                    Icon = "default",
                    Price=9.99F
                },
                new SkinEntity
                {
                    Name = "Summer Daffodil",
                    Description = "A jewel of Summer for all year long",
                    Icon = "default",
                    Price=9.99F
                },
            });
            modelBuilder.Entity<SkillEntity>().HasData(new List<SkillEntity>() {
                new()
                {
                    Name = "Beautiful layout",
                    Description = "Bowl'In",
                    Type = SkillType.Ultimate
                },
                new()
                {
                    Name = "DB Support",
                    Description = "DB",
                    Type = SkillType.Basic
                }
            });
            modelBuilder.Entity<RunePageEntity>().HasData(new List<RunePageEntity>()
            {
                new()
                {
                    Name="FirstRunepage"
                }
            });
            modelBuilder.Entity<RuneEntity>().HasData(new List<RuneEntity>() {
                new()
                {
                    Name = "Mastering of Blue",
                    Description = "Blue shades",
                    Family = RuneFamily.Domination
                },
                new()
                {
                    Name = "Empty Shards",
                    Description = "Remove runes",
                    Family = RuneFamily.Precision
                }
            });
        }
    }
}