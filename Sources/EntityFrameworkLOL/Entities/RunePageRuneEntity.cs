﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Model.RunePage;

namespace EntityFrameworkLOL.Entities
{
    public class RunePageRuneEntity
    {
        public Category Category { get; set; }

        public RuneEntity Rune { get; set; }

        public RunePageEntity RunePage { get; set; }
    }
}