﻿using DTO;
using EntityFrameworkLOL.DBContexts;
using EntityFrameworkLOL.Entities;
using Model;

namespace APILOL.Mapper
{
    public static class RunePageMapper
    {
        public static RunePageDTO ToDto(this RunePage runePage)
        {
            return new RunePageDTO()
            {
                Name = runePage.Name,
                Runes = runePage.Runes
            };
        }

        public static RunePage ToModel(this RunePageDTO runePage)
        {
            return new RunePage(runePage.Name);
        }

        public static RunePage ToModel(this RunePageEntity entity, SQLiteContext context)
        {
            RunePage runePage = new(entity.Name);
            if (entity.Runes != null)
            {
                foreach (var r in entity.Runes)
                {
                    var rune = context.Rune.Find(r.Name);
                    //if (rune != null) runePage[r.category] = rune.ToModel();
                };
            }
            return runePage;
        }

        public static RunePageEntity ToEntity(this RunePage item, SQLiteContext context)
        {
            RunePageEntity? runePageEntity = context.RunePage.Find(item.Name);
            if (runePageEntity == null)
            {
                runePageEntity = new()
                {
                    Name = item.Name,
                };
                runePageEntity.Runes = new List<RuneEntity>();
                foreach (var r in item.Runes)
                {
                    runePageEntity.Runes.Add(r.Value.ToEntity(context));
                }
            }
            return runePageEntity;
        }
    }
}
