﻿using APILOL.Controllers.Request;
using APILOL.Mapper;
using DTO;
using Microsoft.AspNetCore.Mvc;
using Model;
using StubLib;
using System.Xml.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace APILOL.Controllers.v2
{
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("2.0")]

    public class ChampionsController : ControllerBase
    {
        IChampionsManager dataManager;

        private readonly ILogger<ChampionsController> _logger;

        public ChampionsController(IDataManager dataManager, ILogger<ChampionsController> logger)
        {
            this.dataManager = dataManager.ChampionsMgr;
            this._logger = logger;

        }



        // GET: api/<ChampionController>
        [MapToApiVersion("2.0")]
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] PageRequest request)
        {
            _logger.LogInformation("API call - [GET] - CHAMPION ");
            try
            {

                var total = await dataManager.GetNbItems();
                var champions = await dataManager.GetItems(request.Offset, request.Limit, request.OrderingPropertyName, request.IsDesc);
                IEnumerable<ChampionDTO> items = champions.Select(c => c.ToDto());
                if (items.Count() == 0)
                {
                    _logger.LogError("No champion found.");
                    return NotFound("No champion found.");
                }
                return Ok(new { data = items, count = await dataManager.GetNbItems(), offset = request.Offset, limit = request.Limit });
            }
            catch (Exception error)
            {
                _logger.LogInformation("Error in the request");
                return BadRequest(error.Message);
            }

        }

        // GET api/<ChampionController>/5
        [MapToApiVersion("2.0")]
        [HttpGet("{name}")]
        public async Task<IActionResult> Get([FromQuery] PageRequest request, string name)
        {
            _logger.LogInformation("API call - [GET / NAME] - CHAMPION {name}", name);
            try
            {
                if (dataManager.GetNbItemsByName(name) != null)
                {
                    var champions = await dataManager.GetItemsByName(name, request.Offset, request.Limit, request.OrderingPropertyName, request.IsDesc);
                    IEnumerable<ChampionDTO> items = champions.Select(c => c.ToDto());
                    if (items.Count() == 0)
                    {
                        _logger.LogError("No champion found.");
                        return NotFound("No champion found.");
                    }

                    return Ok(new { data = items, count = dataManager.GetNbItems() });
                }
                return NotFound("No champion matching with this name.");
            }
            catch (Exception error)
            {
                _logger.LogInformation("Error in the request");
                return BadRequest(error.Message);
            }
        }

        // POST api/<ChampionController>
        [MapToApiVersion("2.0")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ChampionDTO championDTO)
        {
            _logger.LogInformation("API call - [POST] - CHAMPION");

            try
            {
                if (await dataManager.GetNbItemsByName(championDTO.Name) == 0)
                {
                    await dataManager.AddItem(championDTO.ToModel());
                    return CreatedAtAction(nameof(Get), championDTO);
                }
                _logger.LogError("A champion already exist with this Name. ( Unique Name )");
                return BadRequest("A champion already exist with this Name. ( Unique Name )");
            }
            catch (Exception error)
            {
                _logger.LogInformation("Error in the request");
                return BadRequest(error.Message);
            }
        }

        // PUT api/<ChampionController>/5
        [MapToApiVersion("2.0")]
        [HttpPut("{name}")]
        public async Task<IActionResult> PutAsync(string name, [FromBody] ChampionDTO championDTO)
        {
            _logger.LogInformation("API call - [PUT / NAME] - CHAMPION");
            try
            {
                var champion = await dataManager
                .GetItemsByName(name, 0, await dataManager.GetNbItems());
                Console.WriteLine(champion.First().Name);
                var champion2 = await dataManager
                .GetItemsByName(championDTO.Name, 0, await dataManager.GetNbItems());
                if (champion != null)
                {
                    if (champion2.Count() == 0)
                    {
                        await dataManager.UpdateItem(champion.First(), championDTO.ToModel());
                        return Ok();

                    }
                    _logger.LogError("champion already exist with this unique name.");
                    return BadRequest("champion already exist with this unique name.");

                }
                else
                {
                    _logger.LogError("champion not found.");
                    return NotFound("champion not found.");
                }
                await dataManager.UpdateItem(champion.First(), championDTO.ToModel());
                return Ok();

            }
            catch (Exception e)
            {
                _logger.LogInformation("Error in the request");
                return BadRequest(e.Message);
            }

        }

        // DELETE api/<ChampionController>/5
        [MapToApiVersion("2.0")]
        [HttpDelete("{name}")]
        public async Task<IActionResult> Delete(string name)
        {
            _logger.LogInformation("API call - [DELETE / NAME] - CHAMPION");
            try
            {
                var champion = await (dataManager.GetItemsByName(name, 0, await dataManager.GetNbItems()));

                if (champion.Count() != 0)
                {
                    var championDto = champion.First().ToDto();
                    await dataManager.DeleteItem(champion.First());
                    return Ok(championDto);
                }
                else
                {
                    _logger.LogError("No matching Champion with this name");
                    return NotFound("No matching Champion with this name");
                }

            }
            catch (Exception error)
            {
                _logger.LogInformation("Error in the request");
                return BadRequest(error);
            }
        }
    }
}
