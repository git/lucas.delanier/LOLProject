﻿using Model;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace DTO
{
    public class RunePageDTO
    {
        public string Name { get; set; }
        public ReadOnlyDictionary<RunePage.Category, Rune> Runes { get; set; }
    }
}