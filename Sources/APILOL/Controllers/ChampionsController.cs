﻿using APILOL.Mapper;
using DTO;
using Microsoft.AspNetCore.Mvc;
using Model;
using StubLib;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace APILOL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChampionsController : ControllerBase
    {
        IChampionsManager dataManager;

        public ChampionsController(IDataManager dataManager)
        {
            this.dataManager = dataManager.ChampionsMgr;
        }



        // GET: api/<ChampionController>
        [HttpGet]
        public async Task<IActionResult> Get(int index, int count)
        {
            var champions = await dataManager.GetItems(index, count);
            IEnumerable<ChampionDTO> items = champions.Select(c => c.ToDto());   
            return Ok(items);
        }

        // GET api/<ChampionController>/5
        [HttpGet("{name}")]
        public async Task<IActionResult> Get(string name)
        {
            if (dataManager.GetNbItemsByName(name) != null)
            {
                return Ok(dataManager.GetItemsByName(name, 0, await dataManager.GetNbItems()));
            }
            return NotFound();
        }

        // POST api/<ChampionController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ChampionDTO championDTO)
        {

            return CreatedAtAction(nameof(Get),(await dataManager.AddItem(championDTO.ToModel())).ToDto);
        }

        // PUT api/<ChampionController>/5
        [HttpPut("{name}")]
        public async Task<IActionResult> PutAsync(string name, [FromBody] ChampionDTO championDTO)
        {

            var dtos = (await dataManager.GetItemsByName(name, 0, await dataManager.GetNbItems()));
            
            return Ok(dataManager.UpdateItem(dtos.First(), championDTO.ToModel()));

        }

        // DELETE api/<ChampionController>/5
        [HttpDelete("{name}")]
        public async Task<IActionResult> Delete(string name)
        {
            var dtos = (await dataManager.GetItemsByName(name, 0, await dataManager.GetNbItems()));
            return Ok(dataManager.DeleteItem(dtos.First()));
        }
    }
}