﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class SkinDTO
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Icon {  get; set; }

        public float Price { get; set; }

        public ChampionDTO Champion { get; set; }

        public string Image { get; set; }
    }
}
