﻿using EntityFrameworkLOL.DBContexts;
using EntityFrameworkLOL.Entities;

namespace APILOL.Mapper
{
    public static class CharacteristicsMapper
    {
        public static CharacteristicEntity ToEntity(this KeyValuePair<string, int> item, ChampionEntity champion, SQLiteContext context)
        {
            var characteristicEntity = context.Characteristic.Find(item.Key, champion.Name);
            if (characteristicEntity == null)
            {
                return new()
                {
                    Name = item.Key,
                    Value = item.Value,
                };
            }
            return characteristicEntity;
        }


        public static Tuple<string, int> ToModel(this CharacteristicEntity entity)
        => new(entity.Name, entity.Value);
    }
}
