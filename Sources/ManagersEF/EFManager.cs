﻿using Microsoft.EntityFrameworkCore;
using Model;
using EntityFrameworkLOL.DBContexts;

namespace ManagersEF
{
    public partial class EFManager : IDataManager
    {

        public EFManager(SQLiteContext dbContext)
        {
            DbContext = dbContext;
            ChampionsMgr = new ChampionsManager(this);
            SkinsMgr = new SkinsManager(this);
            RunesMgr = new RunesManager(this);
            RunePagesMgr = new RunePagesManager(this);
        }
        protected SQLiteContext DbContext { get; }

        public IChampionsManager ChampionsMgr { get; }

        public ISkinsManager SkinsMgr { get; }

        public IRunesManager RunesMgr { get; }

        public IRunePagesManager RunePagesMgr { get; }
    }
}