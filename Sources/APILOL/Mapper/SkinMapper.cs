﻿using DTO;
using EntityFrameworkLOL.DBContexts;
using EntityFrameworkLOL.Entities;
using Model;

namespace APILOL.Mapper
{
    public static class SkinMapper
    {
        public static SkinDTO ToDto(this Skin skin)
        {
            return new SkinDTO()
            {
                Name = skin.Name,
                Description = skin.Description,
                Icon = skin.Icon,
                Price = skin.Price,
                Champion = skin.Champion.ToDto(),
                Image = skin.Image.Base64,
            };
        }

        public static Skin ToModel(this SkinDTO skin)
        {
            return new Skin(skin.Name, skin.Champion.ToModel(),skin.Price, skin.Image, skin.Icon, skin.Description);
        }

        public static Skin ToModel(this SkinEntity entity)
        {
            return new Skin(entity.Name, entity.ChampionSkin.ToModel(), entity.Price, entity.Icon, entity.Description);
        }

        public static SkinEntity ToEntity(this Skin item, SQLiteContext? context = null)
        {
            return new()
            {
                Name = item.Name,
                ChampionSkin = context?.Champion.Find(item.Champion.Name) ?? item.Champion.ToEntity(context),
                Description = item.Description,
                Icon = item.Icon,
                Image = null,
                Price = item.Price
            };
        }
    }
}