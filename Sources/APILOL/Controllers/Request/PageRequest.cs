﻿namespace APILOL.Controllers.Request
{
    public class PageRequest
    {
        public bool IsDesc { get; set; } = false;

        public int Offset { get; set; } = 0;

        public string? OrderingPropertyName { get; set; } = "Name";
        public int Limit { get; set; } = 10;

    }
}
