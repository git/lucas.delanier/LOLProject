﻿using APILOL.Controllers.Request;
using APILOL.Mapper;
using DTO;
using Microsoft.AspNetCore.Mvc;
using Model;
using StubLib;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace APILOL.Controllers.v2
{
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("2.0")]
    public class RunePageController : ControllerBase
    {
        IRunePagesManager dataManager;

        private readonly ILogger<RunePageController> _logger;

        public RunePageController(IDataManager dataManager, ILogger<RunePageController> logger)
        {
            this.dataManager = dataManager.RunePagesMgr;
            this._logger = logger;

        }



        // GET: api/<RunePageController>
        [MapToApiVersion("2.0")]
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] PageRequest request)
        {
            _logger.LogInformation("API call - [GET] - RUNEPAGE");
            try
            {

                var total = await dataManager.GetNbItems();
                var runePages = await dataManager.GetItems(request.Offset, request.Limit, request.OrderingPropertyName, request.IsDesc);
                IEnumerable<RunePageDTO> items = runePages.Select(c => c.ToDto());
                if (items.Count() == 0)
                {
                    _logger.LogInformation("No runePage found.");
                    return NotFound("No runePage found.");
                }
                return Ok(new { data = items, count = await dataManager.GetNbItems(), offset = request.Offset, limit = request.Limit });
            }
            catch (Exception error)
            {
                return BadRequest(error.Message);
            }

        }

        // GET api/<RunePageController>/5
        [MapToApiVersion("2.0")]
        [HttpGet("{name}")]
        public async Task<IActionResult> Get([FromQuery] PageRequest request, string name)
        {
            _logger.LogInformation("API call - [GET / NAME] - RUNEPAGE");
            try
            {
                if (dataManager.GetNbItemsByName(name) != null)
                {
                    var runepages = await dataManager.GetItemsByName(name, request.Offset, request.Limit, request.OrderingPropertyName, request.IsDesc);
                    IEnumerable<RunePageDTO> items = runepages.Select(c => c.ToDto());
                    if (items.Count() == 0)
                    {
                        _logger.LogInformation("No runePage found.");
                        return NotFound("No runePage found.");
                    }

                    return Ok(items);
                }
                return NotFound("No runePage matching with this name.");
            }
            catch (Exception error)
            {
                return BadRequest(error.Message);
            }
        }

        // POST api/<RunePageController>
        [MapToApiVersion("2.0")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] RunePageDTO runePageDTO)
        {
            _logger.LogInformation("API call - [POST] - RUNEPAGE");

            try
            {
                if (await dataManager.GetNbItemsByName(runePageDTO.Name) == 0)
                {
                    await dataManager.AddItem(runePageDTO.ToModel());
                    return CreatedAtAction(nameof(Get), runePageDTO);
                }
                _logger.LogInformation("A runePage already exist with this Name. ( Unique Name )");
                return BadRequest("A runePage already exist with this Name. ( Unique Name )");
            }
            catch (Exception error)
            {
                _logger.LogInformation("Error in the request");
                return BadRequest(error.Message);
            }
        }

        // PUT api/<RunePage>/5
        [MapToApiVersion("2.0")]
        [HttpPut("{name}")]
        public async Task<IActionResult> PutAsync(string name, [FromBody] RunePageDTO runePageDTO)
        {
            _logger.LogInformation("API call - [PUT / NAME] - RUNEPAGE");
            try
            {
                var runePage = await dataManager
                .GetItemsByName(name, 0, await dataManager.GetNbItems());
                Console.WriteLine(runePage.First().Name);
                var runePage2 = await dataManager
                .GetItemsByName(runePageDTO.Name, 0, await dataManager.GetNbItems());
                if (runePage != null)
                {
                    if (runePage2.Count() == 0)
                    {
                        await dataManager.UpdateItem(runePage.First(), runePageDTO.ToModel());
                        return Ok();

                    }
                    _logger.LogInformation("runePage already exist with this unique name.");
                    return BadRequest("runePage already exist with this unique name.");

                }
                else
                {
                    _logger.LogInformation("runePage not found.");
                    return NotFound("runePage not found.");
                }
                await dataManager.UpdateItem(runePage.First(), runePageDTO.ToModel());
                return Ok();

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        // DELETE api/<RunePageController>/5
        [MapToApiVersion("2.0")]
        [HttpDelete("{name}")]
        public async Task<IActionResult> Delete(string name)
        {
            _logger.LogInformation("API call - [DELETE / NAME] - RUNEPAGE");
            try
            {
                var runePage = await (dataManager.GetItemsByName(name, 0, await dataManager.GetNbItems()));

                if (runePage.Count() != 0)
                {
                    var runePageDto = runePage.First().ToDto();
                    await dataManager.DeleteItem(runePage.First());
                    return Ok(runePageDto);
                }
                else
                {
                    _logger.LogInformation("No matching runePage with this name");
                    return NotFound("No matching runePage with this name");
                }

            }
            catch (Exception error)
            {
                return BadRequest(error);
            }
        }
    }
}

/*
var champion = new Champion("");
var dto = ChampionMapper.ToDto(champion);
*/