﻿using DTO;
using EntityFrameworkLOL.DBContexts;
using EntityFrameworkLOL.Entities;
using Model;

namespace APILOL.Mapper
{
    public static class SkillMapper
    {
        public static SkillEntity ToEntity(this Skill item, ChampionEntity champion, SQLiteContext context)
        {
            var skillEntity = context.Skill.Find(item.Name);
            if (skillEntity == null)
            {
                return new()
                {
                    Name = item.Name,
                    Description = item.Description,
                    Type = item.Type,
                    Champions = new List<ChampionEntity>() { champion }
                };
            }
            skillEntity!.Champions?.Add(champion);
            return skillEntity;
        }


        public static Skill ToModel(this SkillEntity entity)
        => new(entity.Name, entity.Type, entity.Description);

        public static SkillDTO ToDto(this Skill skill)
        {
            return new SkillDTO()
            {
                Type = skill.Type,
                Name = skill.Name,
                Description = skill.Description
            };
        }

        public static Skill ToModel(this SkillDTO skill)
        {
            return new Skill(skill.Name, skill.Type, skill.Description);
        }
    }
}
