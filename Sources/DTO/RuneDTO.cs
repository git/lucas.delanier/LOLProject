﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace DTO
{
    public class RuneDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public string Image { get; set; }

        public RuneFamily Family { get; set; }
    }
}
