﻿using APILOL.Controllers.Request;
using APILOL.Mapper;
using DTO;
using Microsoft.AspNetCore.Mvc;
using Model;

namespace APILOL.Controllers.v1
{
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1.0")]
    public class SkinController : ControllerBase
    {

        ISkinsManager dataManager;

        private readonly ILogger<SkinController> _logger;

        public SkinController(IDataManager dataManager, ILogger<SkinController> logger)
        {
            this.dataManager = dataManager.SkinsMgr;
            this._logger = logger;

        }



        // GET: api/<SkinController>
        [MapToApiVersion("1.0")]
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] PageRequest request)
        {
            _logger.LogInformation("API call - [GET] - SKIN");
            try
            {

                var total = await dataManager.GetNbItems();
                var skins = await dataManager.GetItems(request.Offset, request.Limit, request.OrderingPropertyName, request.IsDesc);
                IEnumerable<SkinDTO> items = skins.Select(c => c.ToDto());
                if (items.Count() == 0)
                {
                    _logger.LogInformation("No skin found.");
                    return NotFound("No skin found.");
                }
                return Ok(new { data = items, count = await dataManager.GetNbItems(), offset = request.Offset, limit = request.Limit });
            }
            catch (Exception error)
            {
                return BadRequest(error.Message);
            }

        }

        // GET api/<SkinController>/5
        [MapToApiVersion("1.0")]
        [HttpGet("{name}")]
        public async Task<IActionResult> Get([FromQuery] PageRequest request, string name)
        {
            _logger.LogInformation("API call - [GET / NAME] - SKIN");
            try
            {
                if (dataManager.GetNbItemsByName(name) != null)
                {
                    var skins = await dataManager.GetItemsByName(name, request.Offset, request.Limit, request.OrderingPropertyName, request.IsDesc);
                    IEnumerable<SkinDTO> items = skins.Select(c => c.ToDto());
                    if (items.Count() == 0)
                    {
                        _logger.LogInformation("No Skin found.");
                        return NotFound("No Skin found.");
                    }

                    return Ok(items);
                }
                return NotFound("No Skin matching with this name.");
            }
            catch (Exception error)
            {
                return BadRequest(error.Message);
            }
        }

        // POST api/<SkinController>
        [MapToApiVersion("1.0")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] SkinDTO skinDTO)
        {
            _logger.LogInformation("API call - [POST] - SKIN");

            try
            {
                if (await dataManager.GetNbItemsByName(skinDTO.Name) == 0)
                {
                    await dataManager.AddItem(skinDTO.ToModel());
                    return CreatedAtAction(nameof(Get), skinDTO);
                }
                _logger.LogInformation("A Skin already exist with this Name. ( Unique Name )");
                return BadRequest("A Skin already exist with this Name. ( Unique Name )");
            }
            catch (Exception error)
            {
                _logger.LogInformation("Error in the request");
                return BadRequest(error.Message);
            }
        }

        // PUT api/<Skin>/5
        [MapToApiVersion("1.0")]
        [HttpPut("{name}")]
        public async Task<IActionResult> PutAsync(string name, [FromBody] SkinDTO skinDTO)
        {
            _logger.LogInformation("API call - [PUT / NAME] - SKIN");
            try
            {
                var skin = await dataManager
                .GetItemsByName(name, 0, await dataManager.GetNbItems());
                Console.WriteLine(skin.First().Name);
                var skin2 = await dataManager
                .GetItemsByName(skinDTO.Name, 0, await dataManager.GetNbItems());
                if (skin != null)
                {
                    if (skin2.Count() == 0)
                    {
                        await dataManager.UpdateItem(skin.First(), skinDTO.ToModel());
                        return Ok();

                    }
                    _logger.LogInformation("Skin already exist with this unique name.");
                    return BadRequest("Skin already exist with this unique name.");

                }
                else
                {
                    _logger.LogInformation("Skin not found.");
                    return NotFound("Skin not found.");
                }
                await dataManager.UpdateItem(skin.First(), skinDTO.ToModel());
                return Ok();

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        // DELETE api/<SkinController>/5
        [MapToApiVersion("1.0")]
        [HttpDelete("{name}")]
        public async Task<IActionResult> Delete(string name)
        {
            _logger.LogInformation("API call - [DELETE / NAME] - SKIN");
            try
            {
                var skin = await (dataManager.GetItemsByName(name, 0, await dataManager.GetNbItems()));

                if (skin.Count() != 0)
                {
                    var skinDto = skin.First().ToDto();
                    await dataManager.DeleteItem(skin.First());
                    return Ok(skinDto);
                }
                else
                {
                    _logger.LogInformation("No matching skin with this name");
                    return NotFound("No matching skin with this name");
                }

            }
            catch (Exception error)
            {
                return BadRequest(error);
            }
        }
    }
}
