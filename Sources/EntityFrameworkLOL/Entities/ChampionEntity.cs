﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using Model;
using static System.Net.Mime.MediaTypeNames;
using System.Reflection.PortableExecutable;
using System.Security.Claims;
using EntityFrameworkLOL.Entities;

namespace EntityFrameworkLOL.Entities
{
    public class ChampionEntity
    {
        [Key]
        public string Name { get; set; }

        [Required]
        public string Bio { get; set; }

        public string? Icon { get; set; }

        [Required]
        public ChampionClass Class { get; set; }

        public ImageEntity? Image { get; set; }

        public virtual ICollection<SkillEntity> Skills { get; set; }

        public virtual ICollection<CharacteristicEntity> Characteristics { get; set; }

        public virtual ICollection<RunePageEntity> RunePages { get; set; }
    }
}