﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace EntityFrameworkLOL.Migrations
{
    /// <inheritdoc />
    public partial class MigrationWallah6 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Image",
                columns: table => new
                {
                    Base64 = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Image", x => x.Base64);
                });

            migrationBuilder.CreateTable(
                name: "RunePage",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RunePage", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "Skill",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Description = table.Column<string>(type: "TEXT", nullable: false),
                    Type = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skill", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "Champion",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Bio = table.Column<string>(type: "TEXT", nullable: false),
                    Icon = table.Column<string>(type: "TEXT", nullable: true),
                    Class = table.Column<int>(type: "INTEGER", nullable: false),
                    ImageBase64 = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Champion", x => x.Name);
                    table.ForeignKey(
                        name: "FK_Champion_Image_ImageBase64",
                        column: x => x.ImageBase64,
                        principalTable: "Image",
                        principalColumn: "Base64");
                });

            migrationBuilder.CreateTable(
                name: "Rune",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Description = table.Column<string>(type: "TEXT", nullable: false),
                    ImageBase64 = table.Column<string>(type: "TEXT", nullable: true),
                    Family = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rune", x => x.Name);
                    table.ForeignKey(
                        name: "FK_Rune_Image_ImageBase64",
                        column: x => x.ImageBase64,
                        principalTable: "Image",
                        principalColumn: "Base64");
                });

            migrationBuilder.CreateTable(
                name: "ChampionEntityRunePageEntity",
                columns: table => new
                {
                    ChampionsName = table.Column<string>(type: "TEXT", nullable: false),
                    RunePagesName = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChampionEntityRunePageEntity", x => new { x.ChampionsName, x.RunePagesName });
                    table.ForeignKey(
                        name: "FK_ChampionEntityRunePageEntity_Champion_ChampionsName",
                        column: x => x.ChampionsName,
                        principalTable: "Champion",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChampionEntityRunePageEntity_RunePage_RunePagesName",
                        column: x => x.RunePagesName,
                        principalTable: "RunePage",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChampionEntitySkillEntity",
                columns: table => new
                {
                    ChampionsName = table.Column<string>(type: "TEXT", nullable: false),
                    SkillsName = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChampionEntitySkillEntity", x => new { x.ChampionsName, x.SkillsName });
                    table.ForeignKey(
                        name: "FK_ChampionEntitySkillEntity_Champion_ChampionsName",
                        column: x => x.ChampionsName,
                        principalTable: "Champion",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChampionEntitySkillEntity_Skill_SkillsName",
                        column: x => x.SkillsName,
                        principalTable: "Skill",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Characteristic",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Value = table.Column<int>(type: "INTEGER", nullable: false),
                    ChampionName = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characteristic", x => x.Name);
                    table.ForeignKey(
                        name: "FK_Characteristic_Champion_ChampionName",
                        column: x => x.ChampionName,
                        principalTable: "Champion",
                        principalColumn: "Name");
                });

            migrationBuilder.CreateTable(
                name: "Skin",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Description = table.Column<string>(type: "TEXT", nullable: false),
                    Icon = table.Column<string>(type: "TEXT", nullable: true),
                    Price = table.Column<float>(type: "REAL", nullable: false),
                    ImageBase64 = table.Column<string>(type: "TEXT", nullable: true),
                    ChampionSkinName = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skin", x => x.Name);
                    table.ForeignKey(
                        name: "FK_Skin_Champion_ChampionSkinName",
                        column: x => x.ChampionSkinName,
                        principalTable: "Champion",
                        principalColumn: "Name");
                    table.ForeignKey(
                        name: "FK_Skin_Image_ImageBase64",
                        column: x => x.ImageBase64,
                        principalTable: "Image",
                        principalColumn: "Base64");
                });

            migrationBuilder.CreateTable(
                name: "RuneEntityRunePageEntity",
                columns: table => new
                {
                    RunePagesName = table.Column<string>(type: "TEXT", nullable: false),
                    RunesName = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RuneEntityRunePageEntity", x => new { x.RunePagesName, x.RunesName });
                    table.ForeignKey(
                        name: "FK_RuneEntityRunePageEntity_RunePage_RunePagesName",
                        column: x => x.RunePagesName,
                        principalTable: "RunePage",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RuneEntityRunePageEntity_Rune_RunesName",
                        column: x => x.RunesName,
                        principalTable: "Rune",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Champion",
                columns: new[] { "Name", "Bio", "Class", "Icon", "ImageBase64" },
                values: new object[,]
                {
                    { "Jonquille", "Daffodil", 5, "", null },
                    { "WinKer", "Best front-end designer", 3, "", null }
                });

            migrationBuilder.InsertData(
                table: "Characteristic",
                columns: new[] { "Name", "ChampionName", "Value" },
                values: new object[,]
                {
                    { "Back-end", null, 100 },
                    { "Front-end", null, 100 }
                });

            migrationBuilder.InsertData(
                table: "Image",
                column: "Base64",
                value: "default");

            migrationBuilder.InsertData(
                table: "Rune",
                columns: new[] { "Name", "Description", "Family", "ImageBase64" },
                values: new object[,]
                {
                    { "Empty Shards", "Remove runes", 1, null },
                    { "Mastering of Blue", "Blue shades", 2, null }
                });

            migrationBuilder.InsertData(
                table: "RunePage",
                column: "Name",
                value: "FirstRunepage");

            migrationBuilder.InsertData(
                table: "Skill",
                columns: new[] { "Name", "Description", "Type" },
                values: new object[,]
                {
                    { "Beautiful layout", "Bowl'In", 3 },
                    { "DB Support", "DB", 1 }
                });

            migrationBuilder.InsertData(
                table: "Skin",
                columns: new[] { "Name", "ChampionSkinName", "Description", "Icon", "ImageBase64", "Price" },
                values: new object[,]
                {
                    { "Darker WinKer", null, "Be invisible in the darkness but never alone", "default", null, 9.99f },
                    { "Summer Daffodil", null, "A jewel of Summer for all year long", "default", null, 9.99f }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Champion_ImageBase64",
                table: "Champion",
                column: "ImageBase64");

            migrationBuilder.CreateIndex(
                name: "IX_ChampionEntityRunePageEntity_RunePagesName",
                table: "ChampionEntityRunePageEntity",
                column: "RunePagesName");

            migrationBuilder.CreateIndex(
                name: "IX_ChampionEntitySkillEntity_SkillsName",
                table: "ChampionEntitySkillEntity",
                column: "SkillsName");

            migrationBuilder.CreateIndex(
                name: "IX_Characteristic_ChampionName",
                table: "Characteristic",
                column: "ChampionName");

            migrationBuilder.CreateIndex(
                name: "IX_Rune_ImageBase64",
                table: "Rune",
                column: "ImageBase64");

            migrationBuilder.CreateIndex(
                name: "IX_RuneEntityRunePageEntity_RunesName",
                table: "RuneEntityRunePageEntity",
                column: "RunesName");

            migrationBuilder.CreateIndex(
                name: "IX_Skin_ChampionSkinName",
                table: "Skin",
                column: "ChampionSkinName");

            migrationBuilder.CreateIndex(
                name: "IX_Skin_ImageBase64",
                table: "Skin",
                column: "ImageBase64");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ChampionEntityRunePageEntity");

            migrationBuilder.DropTable(
                name: "ChampionEntitySkillEntity");

            migrationBuilder.DropTable(
                name: "Characteristic");

            migrationBuilder.DropTable(
                name: "RuneEntityRunePageEntity");

            migrationBuilder.DropTable(
                name: "Skin");

            migrationBuilder.DropTable(
                name: "Skill");

            migrationBuilder.DropTable(
                name: "RunePage");

            migrationBuilder.DropTable(
                name: "Rune");

            migrationBuilder.DropTable(
                name: "Champion");

            migrationBuilder.DropTable(
                name: "Image");
        }
    }
}
