﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using Model;
using static System.Net.Mime.MediaTypeNames;
using System.Reflection.PortableExecutable;
using System.Security.Claims;

namespace EntityFrameworkLOL.Entities
{
    public class ImageEntity
    {
        [Key]
        [ForeignKey("ChampionEntity")]
        public string Base64 { get; set; }
    }
}