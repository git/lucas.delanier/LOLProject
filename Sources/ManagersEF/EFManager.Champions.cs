﻿using Model;
using Shared;
using System.Data.SqlTypes;
using APILOL.Mapper;
using Microsoft.EntityFrameworkCore;
using System.Xml.Linq;

namespace ManagersEF
{
    public partial class EFManager
    {
        public class ChampionsManager : IChampionsManager
        {
            private readonly EFManager parent;

            public ChampionsManager(EFManager parent)
                => this.parent = parent;

            public async Task<Champion?> AddItem(Champion? item)
            {
                await parent.DbContext.Champion.AddAsync(item.ToEntity(parent.DbContext));
                parent.DbContext.SaveChanges();
                return item;
            }

            public async Task<bool> DeleteItem(Champion? item)
            {
                var toDelete = parent.DbContext.Champion.Find(item.Name);
                if (toDelete != null)
                {
                    parent.DbContext.Champion.Remove(toDelete);
                    parent.DbContext.SaveChanges();
                    return true;
                }
                return false;
            }

            public async Task<IEnumerable<Champion?>> GetItems(int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                Console.WriteLine("GET");
                return parent.DbContext.Champion.GetItemsWithFilterAndOrdering(
                        c => true,
                        index, count,
                        orderingPropertyName, descending).Select(c => c.ToModel());
            }

            public async Task<IEnumerable<Champion?>> GetItemsByCharacteristic(string charName, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {

                return parent.DbContext.Champion.Include("Characteristics").GetItemsWithFilterAndOrdering(
                    c => c.Characteristics.Any(ch => ch.Name.Equals(charName)),
                    index, count,
                    orderingPropertyName, descending).Select(c => c.ToModel());
            }

            public async Task<IEnumerable<Champion?>> GetItemsByClass(ChampionClass championClass, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.Champion.GetItemsWithFilterAndOrdering(
                    c => c.Class.Equals(championClass),
                    index, count,
                    orderingPropertyName, descending).Select(c => c.ToModel());
            }

            public async Task<IEnumerable<Champion?>> GetItemsByName(string substring, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.Champion.GetItemsWithFilterAndOrdering(
                        c => c.Name.Contains(substring),
                        index, count,
                        orderingPropertyName, descending).Select(c => c.ToModel());
            }

            public async Task<IEnumerable<Champion?>> GetItemsByRunePage(RunePage? runePage, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {

                return parent.DbContext.Champion.Include("runepages").GetItemsWithFilterAndOrdering(
                        c => c.RunePages.Any(rp => rp.Equals(runePage.ToEntity(parent.DbContext))),
                        index, count,
                        orderingPropertyName, descending).Select(c => c.ToModel());

            }

            public async Task<IEnumerable<Champion?>> GetItemsBySkill(Skill? skill, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.Champion.GetItemsWithFilterAndOrdering(
                        c => skill != null && c.Skills.Any(s => s.Name.Equals(skill.Name)),
                        index, count,
                        orderingPropertyName, descending).Select(c => c.ToModel());
            }

            public async Task<IEnumerable<Champion?>> GetItemsBySkill(string skill, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.Champion.Include("Skills").GetItemsWithFilterAndOrdering(
                    c => skill != null && c.Skills.Any(s => s.Name.Equals(skill)),
                    index, count,
                    orderingPropertyName, descending).Select(c => c.ToModel());
            }

            public async Task<int> GetNbItems()
            {
                return parent.DbContext.Champion.Count();
            }

            public async Task<int> GetNbItemsByCharacteristic(string charName)
            {
                return parent.DbContext.Champion.Where(c => c.Characteristics.Any(ch => ch.Name.Equals(charName))).Count();
            }

            public async Task<int> GetNbItemsByClass(ChampionClass championClass)
            {
                return parent.DbContext.Champion.Where(c => c.Class.Equals(championClass))
                .Count();
            }

            public async Task<int> GetNbItemsByName(string substring)
            {
                return parent.DbContext.Champion.Where(c => c.Name.Contains(substring))
                .Count();
            }

            public async Task<int> GetNbItemsByRunePage(RunePage? runePage)
            {
                return parent.DbContext.Champion.Where(c => c.RunePages.Any(rp => rp.Equals(runePage.ToEntity(parent.DbContext)))).Count();

            }

            public async Task<int> GetNbItemsBySkill(Skill? skill)
            {
                return parent.DbContext.Champion.Where(c => skill != null && c.Skills.Any(s => s.Name.Equals(skill.Name)))
                    .Count();
            }

            public async Task<int> GetNbItemsBySkill(string skill)
            {
                return parent.DbContext.Champion.Where(c => skill != null && c.Skills.Any(s => s.Name.Equals(skill)))
                    .Count();
            }

            public async Task<Champion?> UpdateItem(Champion? oldItem, Champion? newItem)
            {
                var toUpdate = parent.DbContext.Champion.Find(oldItem.Name);
                toUpdate = newItem.ToEntity(parent.DbContext);
                parent.DbContext.SaveChanges();
                return newItem;
            }
        }
    }
}