﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Model;

namespace EntityFrameworkLOL.Entities
{
    public class RuneEntity
    {
        [Key]
        public string Name { get; set; }

        public string Description { get; set; }

        public ImageEntity Image { get; set; }

        [Required]
        public RuneFamily Family { get; set; }

        public virtual ICollection<RunePageEntity> RunePages { get; set; }
    }
}