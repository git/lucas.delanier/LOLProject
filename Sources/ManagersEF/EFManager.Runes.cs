﻿using APILOL.Mapper;
using Microsoft.EntityFrameworkCore;
using Model;
using Shared;
using System.Xml.Linq;

namespace ManagersEF
{
    public partial class EFManager
    {
        public class RunesManager : IRunesManager
        {
            private readonly EFManager parent;

            public RunesManager(EFManager parent)
                => this.parent = parent;
            public async Task<Rune?> AddItem(Rune? item)
            {
                await parent.DbContext.Rune.AddAsync(item.ToEntity(parent.DbContext));
                parent.DbContext.SaveChanges();
                return item;
            }

            public async Task<bool> DeleteItem(Rune? item)
            {
                var toDelete = parent.DbContext.Rune.Find(item?.Name);
                if (toDelete != null)
                {
                    parent.DbContext.Rune.Remove(item?.ToEntity(parent.DbContext));
                    parent.DbContext.SaveChanges();
                    return true;
                }
                return false;
            }

            public async Task<IEnumerable<Rune?>> GetItems(int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.Rune.GetItemsWithFilterAndOrdering(
                        r => true,
                        index, count,
                        orderingPropertyName, descending).Select(r => r.ToModel());
            }

            public async Task<IEnumerable<Rune?>> GetItemsByFamily(RuneFamily family, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.Rune.GetItemsWithFilterAndOrdering(
                    r => r.Family.Equals(family),
                    index, count,
                    orderingPropertyName, descending).Select(r => r.ToModel());
            }

            public async Task<IEnumerable<Rune?>> GetItemsByName(string substring, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.Rune.GetItemsWithFilterAndOrdering(
                    r => r.Name.Contains(substring),
                    index, count,
                    orderingPropertyName, descending).Select(r => r.ToModel());
            }

            public async Task<int> GetNbItems()
            {
                return parent.DbContext.Rune.Count();
            }

            public async Task<int> GetNbItemsByFamily(RuneFamily family)
            {
                return parent.DbContext.Rune.Where(r => r.Family.Equals(family)).Count();
            }

            public async Task<int> GetNbItemsByName(string substring)
            {
                return parent.DbContext.Rune.Where(r => r.Name.Contains(substring)).Count();
            }

            public async Task<Rune?> UpdateItem(Rune? oldItem, Rune? newItem)
            {
                var toUpdate = parent.DbContext.Rune.Find(oldItem.Name);
                toUpdate.Description = newItem.Description;
                toUpdate.Family = newItem.Family;
                parent.DbContext.SaveChanges();
                return newItem;
            }
        }
    }
}