﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace EntityFrameworkLOL.Entities
{
    public class SkinEntity
    {
        [Key]
        public string Name { get; set; }

        public string Description { get; set; }

        public string? Icon { get; set; }

        public float Price { get; set; }

        public ImageEntity Image { get; set; }

        public ChampionEntity ChampionSkin { get; set; }
    }
}