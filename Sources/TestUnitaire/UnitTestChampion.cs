using APILOL.Controllers;
using APILOL.Controllers.Request;
using APILOL.Controllers.v1;
using APILOL.Controllers.v2;
using APILOL.Mapper;
using DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using Model;
using StubLib;
using System.Web.Http;
using ChampionsController = APILOL.Controllers.v1.ChampionsController;

namespace TestUnitaire
{
    [TestClass]
    public class UnitTestChampion
    {
        private readonly ChampionsController controller;
        private readonly StubData stub;
        public UnitTestChampion()
        {
            stub = new StubData();
            controller = new ChampionsController(stub, new NullLogger<ChampionsController>());
        }

        [TestMethod]
        public async Task TestGet()
        {
            var champions = await controller.Get(new PageRequest());

            //Assert
            var resultObject = champions as OkObjectResult;
            Assert.IsNotNull(resultObject);

            var resultType = resultObject?.Value as IEnumerable<ChampionDTO>;
            Assert.IsNotNull(resultType);

            Assert.AreEqual(resultType.Count(), await stub.ChampionsMgr.GetNbItems());
        }


        [TestMethod]
        public async Task TestPost()
        {

            //Arange
            var champion = new ChampionDTO
            {
                Name = "Jinx",
                Bio = "Awesome , great, fantastic Q",
            };

            //Act
            var championsResult = await controller.Post(champion);

            //Assert
            var objectResult = championsResult as CreatedAtActionResult;
            Assert.IsNotNull(objectResult);

            var champions = objectResult?.Value as ChampionDTO;
            Assert.IsNotNull(champions);
        }

        [TestMethod]
        public async Task TestDelete()
        {
            //Arange
            string championName = "Aatrox";


            // Act
            var result = await controller.Delete(championName);

            // Assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.IsTrue((await stub.ChampionsMgr.GetItemsByName(championName, 0, await stub.ChampionsMgr.GetNbItems())).Count() == 0);
        }

        [TestMethod]
        public async Task TestUpdate()
        {
            //Arange
            string championName = "Aatrox";
            var updatedChampion = new ChampionDTO {Name = "Bibouuu", Bio = "Updated Bio" };


            // Act
            var result = await controller.PutAsync(championName, updatedChampion);

            // Assert
            Assert.IsInstanceOfType(result, typeof(OkResult));
            Assert.IsNotNull(stub.ChampionsMgr.GetItemsByName("Bibouuu",0, await stub.ChampionsMgr.GetNbItems()));
        }


    }
}