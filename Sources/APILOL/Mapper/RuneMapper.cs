﻿using DTO;
using EntityFrameworkLOL.DBContexts;
using EntityFrameworkLOL.Entities;
using Model;

namespace APILOL.Mapper
{
    public static class RuneMapper
    {
        public static RuneDTO ToDto(this Rune rune)
        {
            return new RuneDTO()
            {
                Name = rune.Name,
                Description = rune.Description,
                Image = rune.Image.Base64,
                Family = rune.Family,
            };
        }

        public static Rune ToModel(this RuneDTO rune)
        {
            return new Rune(rune.Name, rune.Family, rune.Image, rune.Image, rune.Description);
        }

        public static Rune ToModel(this RuneEntity entity)
        {
            return new Rune(entity.Name, entity.Family, "", entity.Image.Base64, entity.Description);
        }

        public static RuneEntity ToEntity(this Rune item, SQLiteContext context)
        {
            RuneEntity? runeEntity = context.Rune.Find(item.Name);
            if (runeEntity == null)
            {
                return new()
                {
                    Name = item.Name,
                    Description = item.Description,
                    Family = item.Family
                };
            }
            return runeEntity;
        }
    }
}
