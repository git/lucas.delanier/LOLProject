﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model;

namespace EntityFrameworkLOL.Entities
{
    public class SkillEntity
    {
        [Key]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public SkillType Type { get; set; }

        public virtual ICollection<ChampionEntity> Champions { get; set; }
    }
}