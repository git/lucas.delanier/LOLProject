﻿using APILOL.Mapper;
using EntityFrameworkLOL.Entities;
using ManagersEF;
using Microsoft.EntityFrameworkCore;
using Model;
using System.Data.SqlTypes;
using System.Linq;
using System.Xml.Linq;

namespace ManagersEF
{
    public partial class EFManager
    {
        public class RunePagesManager : IRunePagesManager
        {
            private readonly EFManager parent;

            public RunePagesManager(EFManager parent)
                => this.parent = parent;

            public async Task<RunePage?> AddItem(RunePage? item)
            {
                await parent.DbContext.RunePage.AddAsync(item.ToEntity(parent.DbContext));
                parent.DbContext.SaveChanges();
                return item;
            }

            public async Task<bool> DeleteItem(RunePage? item)
            {
                var toDelete = parent.DbContext.RunePage.Find(item.Name);
                if (toDelete != null)
                {
                    parent.DbContext.RunePage.Remove(toDelete);
                    parent.DbContext.SaveChanges();
                    return true;
                }
                return false;
            }

            public async Task<IEnumerable<RunePage?>> GetItems(int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.RunePage.GetItemsWithFilterAndOrdering(
                        rp => true,
                        index, count,
                        orderingPropertyName, descending).Select(rp => rp.ToModel(parent.DbContext));
            }

            public async Task<IEnumerable<RunePage?>> GetItemsByChampion(Champion? champion, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.RunePage.Include("champions").GetItemsWithFilterAndOrdering(
                    rp => rp.Champions.Any(c => c.Name.Equals(champion.Name)),
                    index, count,
                    orderingPropertyName, descending).Select(rp => rp.ToModel(parent.DbContext));
            }

            public async Task<IEnumerable<RunePage?>> GetItemsByName(string substring, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.RunePage.GetItemsWithFilterAndOrdering(
                    rp => rp.Name.Contains(substring),
                    index, count,
                    orderingPropertyName, descending).Select(rp => rp.ToModel(parent.DbContext));
            }

            public async Task<IEnumerable<RunePage?>> GetItemsByRune(Rune? rune, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.RunePage.Include("entries").GetItemsWithFilterAndOrdering(
                    rp => rp.entries.Any(r => r.RuneName.Equals(rune.Name)),
                    index, count,
                    orderingPropertyName, descending).Select(rp => rp.ToModel(parent.DbContext));
            }

            public async Task<int> GetNbItems()
            {
                return parent.DbContext.RunePage.Count();

            }

            public async Task<int> GetNbItemsByChampion(Champion? champion)
            {
                return parent.DbContext.RunePage.Where(rp => rp.champions.Any(c => c.Name.Equals(champion.Name))).Count();

            }

            public async Task<int> GetNbItemsByName(string substring)
            {
                return parent.DbContext.RunePage.Where(rp => rp.Name.Contains(substring)).Count();
            }

            public async Task<int> GetNbItemsByRune(Model.Rune? rune)
            {
                return parent.DbContext.RunePage.Where(rp => rp.entries.Any(r => r.RuneName.Equals(rune.Name))).Count();
            }

            public async Task<RunePage?> UpdateItem(RunePage? oldItem, RunePage? newItem)
            {
                var toUpdate = parent.DbContext.RunePage.Include("entries")
                    .Where(x => x.Name.Equals(newItem.Name)).First();
                try
                {
                    toUpdate.entries = newItem.Runes.Select(r => new RunePageRuneEntity()
                    {
                        category = r.Key,
                        rune = r.Value.ToEntity(parent.DbContext),
                    }).ToList();

                    parent.DbContext.SaveChanges();

                }
                catch (DbUpdateException) { }
                return newItem;

            }
        }
    }
}