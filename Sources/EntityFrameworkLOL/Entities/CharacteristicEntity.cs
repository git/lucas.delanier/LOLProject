﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFrameworkLOL.Entities;

namespace EntityFrameworkLOL.Entities
{
    public class CharacteristicEntity
    {
        [Key]
        public string Name { get; set; }

        [Required]
        public int Value { get; set; }

        public ChampionEntity Champion { get; set; }

        //public virtual ICollection<ChampionEntity> Champions { get; set; }
    }
}