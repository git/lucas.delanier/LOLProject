 
  ## LOLProject




**LOLProject** est un projet reliant une API C# et EntityFramework afin de produire une API qui renvoie les informations d'une base de données SQLite sur un client MAUI

Merci de noter la v1 de l'api qui est tenu a jour la v2 sert uniquement a montrer que nous pouvons versionner.

## :floppy_disk: FEATURES

- L'API dispose des principales requêtes CRUD sur les champions, skills, skins, runes, runepages aussi consultables sur le swagger UI. 
- L'ORM réalisé avec EntityFramework afin d'enregistrer dans une base de données SQLite
- L'application MAUI pour faire des requêtes depuis un client et les afficher.


![](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/rainbow.png)

## :dizzy: Getting Started

Une fois le dépot cloné, vous pouvez lancer le code sur votre téléphone Android grâce à l'outil Android Studio ou grâce à un émulateur Android.


## Architecture

![](https://codefirst.iut.uca.fr/git/lucas.delanier/LOLProject/raw/branch/master/Documentations/Schema_architecture.png)
Ce schéma d'architecure globale permet de comprendre la composition interne de la solution.

Sur la partie gauche on peut voir la partie client qui fait appel a l'api via des requetes http en utilisant les routes de l'api Rest Full. 
Pour tout a fait comprendre, comment communiquent et fonctionnent les différentes briques de la solution nous allons vous détailler brique par brique, relation par relation les différentes point rouge du schéma.

## 1  - La déserialization
Le client est une solution qui va intérragir avec notre api grace des requetes sur internet. Il nécessite donc impérativement d'une connexion internet afin de pouvoir communiquer grace a des requetes http(s). Une fois qu'il a récupéré les données demandé. Il doit transoformer ce document json en classe métier de son model. Cela s'appel la déserialization. Ce processus ce traduit ar le code suivant en C#.

![](https://codefirst.iut.uca.fr/git/lucas.delanier/LOLProject/raw/branch/master/Documentations/Capture%20d%27%C3%A9cran%202023-03-26%20111526.png)


## 2  - La communication Client - Api
 Maintenant que vous savez comment le client traite les données renvoyées par l'API, nous allons a présent voir comment ils communiquent entre eux. Cette communication se fait par des requetes http(s) sous la forme suivante.

"codefirst.iut.uca.fr/containers/lucasdelanier-containerlol/api/v{version}/{controller}"

Lors de l'appel de l'API, le client peut choisir la version ( v1, v2, etc) mais aussi indiquer le controlleur qu'il souhaite intérroger. Par exemple Champion lui permettra de récuperer des informations sur les champions que connait l'API. Il est aussi possible de rajouter de précisions en ajoutant par exemple un nom apres le controller pour demander les informations spécifique a un champion en particulié. Lors des communications, les objects sons traduit sous la forme DTO. Cela permet de controller l'envoie de certaines données au client comme pr exemple un id unique ou des données sensibles qui ne doivent pas etre connu par l'utilisateur. Voici un exemple.

![](https://codefirst.iut.uca.fr/git/lucas.delanier/LOLProject/raw/branch/master/Documentations/Capture%20d%27%C3%A9cran%202023-03-26%20113439.png)

Ci dessus on voit que l'object champion a une nouvelle "forme" ChampionDTO que l'on construit a partir des attributs d'un champion. Dans l'exemple, il reprend les memes informations qu'un champion mais on aurait put imaginer qu'il en perde certains que l'on ne veut pas renvoyer a l'utilisateur. Pour permettre le passage d'un DTO en object et d'un object en DTO il faut ce qu'on appel des Mappers. Voici l'exemple des méthodes ToModel() et ToDTO()

![](https://codefirst.iut.uca.fr/git/lucas.delanier/LOLProject/raw/branch/master/Documentations/Capture%20d%27%C3%A9cran%202023-03-26%20113710.png)

## 3  - L'utilisation du stub dans l'API
 Avant de faire la relation avec une base de données. Il était impotant de pouvoir tester l'application avec un Stub qui est une classe contenant des listes d'object. Une sorte de base de données fictive. Pour permettre a l'api d'intéroger ce stub, il a fallut faire un manager spécialement destiné à l'utilisation du stub (StubData) qui hérite d'un manager commun qui est une interface composé des méthodes nécessaire a l'interrogation des données (IDataManager).

 ![](https://codefirst.iut.uca.fr/git/lucas.delanier/LOLProject/raw/branch/master/Documentations/Capture%20d%27%C3%A9cran%202023-03-26%20114811.png)


 ## 4  - Lien entre API et Entityframework
  Pour relier l'API avec la partie EntityFramework, cela se passe du côté de l'API. En effet, en ayant implémenter des managers en EntityFramework, il suffit de les instancier côté API puis de demander la requête adéquate pour obtenir les données de la base de données avec l'API. Ces managers regroupent plusieurs petits managers gérant chacun une entité de la base de données (Champion, RunePage, Rune, ...). Il n'y a pas de managers de Skill car ceux-ci sont compris dans le manager Champion (un peu comme les mappers côté API).


 ## 5  - Mappage des classes métier en tables
 Afin de rentrer les classes métiers en base de données, il faut avant tout les mapper. Ainsi, il y a un mapper pour chaque entité de la base de données (même pour Skill par exemple). Ensuite, il faut deux méthodes pour chaque mapper : 
 <br> 1- ToEntity : elle prend en paramètre un objet du modèle et permet de le transformer en entité afin de pouvoir l'insérer en base de données.
 <br> 2- ToModel : elle prend en paramètre une entité et permet de la transformer en objet.
 Grâce à ces deux méthodes, nous pouvons facilement mapper des entités en objet et inversement.
 

 ## 6  - Lien avec base de données SQLite
 Il y a deux manières d'utiliser la base de données SQLite. On peut soit, utiliser la base de données interne à l'appareil ou bien utiliser la mémoire de l'appareil pour sauvegarder les données en local. Pour créer la base, il faut avoir des classes entités représentant chacune une table de la base. Il peut aussi y avoir des entités représentant des liaisons entre deux tables (RunePageRuneEntity par exemple représente la table entre RunePage et Rune). Ensuite, il faut un contexte (SQLiteContext pour notre projet) qui a pour attributs des DbSets, des listes d'entités à mettre en base de données. Pour remplir la base de données, avec un Stub par exemple, on peut utiliser la méthode onCreate du contexte pour insérer des entités créees manuellement. Cependant, pour mettre des objets en base, il faut donc les mapper en entité puis, passer par un manager (dans notre cas) pour enfin le rentrer en base.


## :wrench: SUPPORT
En cas de problème lors de l'utilisation de l'application, vous pouvez nous contacter aux adresses suivantes :


Lucas Delanier : **Lucas.DELANIER@etu.uca.fr** </br>
Louison Parant : **Louison.PARANT@etu.uca.fr**
![](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/rainbow.png)



## ✨ Contributors 

<a href = "https://codefirst.iut.uca.fr/git/lucas.delanier">
<img src ="https://codefirst.iut.uca.fr/git/avatars/6a3835d734392fccff3949f7c82a63b9?size=870" height="50px">
</a>
<a href = "https://codefirst.iut.uca.fr/git/louison.parant">
<img src ="https://codefirst.iut.uca.fr/git/avatars/b337a607f680a2d9af25eb09ea457be9?size=870" height="50px">
</a>






                                                        