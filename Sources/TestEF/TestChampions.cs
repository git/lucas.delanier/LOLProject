﻿using EntityFrameworkLOL.Entities;
using EntityFrameworkLOL.DBContexts;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model;
using Shared;
using System.Xml.Linq;
using Xunit.Abstractions;
using Xunit;
using ManagersEF;

namespace TestEF
{
    public class TestChampions
    {

        [Fact]
        public async void Test_Add()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<SQLiteContext>()
            .UseSqlite(connection)
            .Options;

            using (var context = new SQLiteContext(options))
            {
                var manager = new EFManager(context).ChampionsMgr;

                context.Database.EnsureCreated();

                Champion batman = new("Batman", ChampionClass.Assassin, "icon_1", "image_1", "L'ombre de la nuit");
                batman.AddSkill(new("Bat-signal", SkillType.Basic, "Envoie le signal"));
                batman.AddCharacteristics(new[] {
                    Tuple.Create("Force", 150),
                    Tuple.Create("Agilité", 500)
                });
                Champion endeavor = new("Endeavor", ChampionClass.Tank, "icon_2", "image_2", "Feu brûlant énernel");
                endeavor.AddSkill(new("Final flames", SkillType.Ultimate, "Dernière flamme d'un héro"));
                endeavor.AddCharacteristics(new[] {
                    Tuple.Create("Force", 200),
                    Tuple.Create("Défense", 300),
                    Tuple.Create("Alter", 800)
                });


                Champion escanor = new("Escanor", ChampionClass.Fighter, "icon_3", "image_3", "1, 2, 3, Soleil");
                escanor.AddSkill(new("Croissance solaire", SkillType.Passive, "Le soleil rends plus fort !"));
                escanor.AddCharacteristics(new[] {
                    Tuple.Create("Force", 500),
                    Tuple.Create("Défense", 500)
                });

                await manager.AddItem(batman);
                await manager.AddItem(endeavor);
                await manager.AddItem(escanor);
            }

            using (var context = new SQLiteContext(options))
            {
                var manager = new EFManager(context).ChampionsMgr;

                context.Database.EnsureCreated();

                var nbItems = await manager.GetNbItems();
                Assert.Equal(3, nbItems);

                var items = await manager.GetItemsByName("Batman", 0, nbItems);
                Assert.Equal("Batman", items.First().Name);


                Assert.Equal(2, await manager.GetNbItemsByName("E"));

                items = await manager.GetItemsBySkill("Croissance solaire", 0, nbItems);
                Assert.Equal("Escanor", items.First().Name);

                items = await manager.GetItemsBySkill(new Skill("Final flames", SkillType.Ultimate, "Dernière flamme d'un héro"),
                                                      0, nbItems);
                Assert.Equal("Endeavor", items.First().Name);

                items = await manager.GetItemsByCharacteristic("Alter", 0, nbItems);
                Assert.Equal("Endeavor", items.First().Name);

                Assert.Equal(2, await manager.GetNbItemsByCharacteristic("Défense"));
            }

        }

        [Fact]
        public async void Test_Update()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<SQLiteContext>()
                .UseSqlite(connection)
                .Options;

            using (var context = new SQLiteContext(options))
            {
                var manager = new EFManager(context).ChampionsMgr;

                context.Database.EnsureCreated();

                Champion batman = new("Batman", ChampionClass.Assassin, "icon_1", "image_1", "L'ombre de la nuit");
                Champion endeavor = new("Endeavor", ChampionClass.Tank, "icon_2", "image_2", "Feu brûlant énernel");
                Champion escanor = new("Escanor", ChampionClass.Fighter, "icon_3", "image_3", "1, 2, 3, Soleil");

                await manager.AddItem(batman);
                await manager.AddItem(endeavor);
                await manager.AddItem(escanor);
            }

            using (var context = new SQLiteContext(options))
            {
                var manager = new EFManager(context).ChampionsMgr;

                context.Database.EnsureCreated();


                var items = await manager.GetItemsByName("E", 0, 3);
                Assert.Equal(2, items.Count());

                var escanor = context.Champion.Where(n => n.Name.Contains("Esc")).First();
                escanor.Class = ChampionClass.Tank;
                context.SaveChanges();

                items = await manager.GetItemsByClass(ChampionClass.Tank, 0, 3);
                Assert.Contains("Escanor", items.Select(x => x.Name));

                Assert.Equal(2, await manager.GetNbItemsByClass(ChampionClass.Tank));

            }
        }

        [Fact]
        public async void Test_Delete()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<SQLiteContext>()
                .UseSqlite(connection)
                .Options;

            using (var context = new SQLiteContext(options))
            {
                var manager = new EFManager(context).ChampionsMgr;

                context.Database.EnsureCreated();


                Champion batman = new("Batman", ChampionClass.Assassin, "icon_1", "image_1", "L'ombre de la nuit");
                batman.AddSkill(new("Charge", SkillType.Basic, "Coup de base"));
                batman.AddSkill(new("Double Saut", SkillType.Basic, ""));

                Champion endeavor = new("Endeavor", ChampionClass.Tank, "icon_2", "image_2", "Feu brûlant énernel");
                endeavor.AddSkill(new("Charge", SkillType.Basic, "Coup de base"));

                Champion escanor = new("Escanor", ChampionClass.Fighter, "icon_3", "image_3", "1, 2, 3, Soleil");
                escanor.AddSkill(new("Charge", SkillType.Basic, "Coup de base"));
                batman.AddSkill(new("Double Saut", SkillType.Basic, ""));

                await manager.AddItem(batman);
                await manager.AddItem(endeavor);
                await manager.AddItem(escanor);
            }

            using (var context = new SQLiteContext(options))
            {
                var manager = new EFManager(context).ChampionsMgr;

                context.Database.EnsureCreated();

                var endeavor = (await manager.GetItemsByName("Endeavor", 0, 3)).First();

                var itemsByName = await manager.DeleteItem(endeavor);
                Assert.Equal(2, await manager.GetNbItems());

                var items = await manager.GetItems(0, await manager.GetNbItems());
                Assert.DoesNotContain("Endeavor", items.Select(x => x.Name));

                Assert.Equal(1, await manager.GetNbItemsBySkill(new Skill("Double Saut", SkillType.Basic, "")));
                Assert.Equal(2, await manager.GetNbItemsBySkill("Charge"));
            }
        }
    }
}