﻿// <auto-generated />
using EntityFrameworkLOL.DBContexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace EntityFrameworkLOL.Migrations
{
    [DbContext(typeof(SQLiteContext))]
    [Migration("20230315161305_MigrationWallah6")]
    partial class MigrationWallah6
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder.HasAnnotation("ProductVersion", "7.0.2");

            modelBuilder.Entity("ChampionEntityRunePageEntity", b =>
                {
                    b.Property<string>("ChampionsName")
                        .HasColumnType("TEXT");

                    b.Property<string>("RunePagesName")
                        .HasColumnType("TEXT");

                    b.HasKey("ChampionsName", "RunePagesName");

                    b.HasIndex("RunePagesName");

                    b.ToTable("ChampionEntityRunePageEntity");
                });

            modelBuilder.Entity("ChampionEntitySkillEntity", b =>
                {
                    b.Property<string>("ChampionsName")
                        .HasColumnType("TEXT");

                    b.Property<string>("SkillsName")
                        .HasColumnType("TEXT");

                    b.HasKey("ChampionsName", "SkillsName");

                    b.HasIndex("SkillsName");

                    b.ToTable("ChampionEntitySkillEntity");
                });

            modelBuilder.Entity("EntityFrameworkLOL.Entities.ChampionEntity", b =>
                {
                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<string>("Bio")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<int>("Class")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Icon")
                        .HasColumnType("TEXT");

                    b.Property<string>("ImageBase64")
                        .HasColumnType("TEXT");

                    b.HasKey("Name");

                    b.HasIndex("ImageBase64");

                    b.ToTable("Champion");

                    b.HasData(
                        new
                        {
                            Name = "WinKer",
                            Bio = "Best front-end designer",
                            Class = 3,
                            Icon = ""
                        },
                        new
                        {
                            Name = "Jonquille",
                            Bio = "Daffodil",
                            Class = 5,
                            Icon = ""
                        });
                });

            modelBuilder.Entity("EntityFrameworkLOL.Entities.CharacteristicEntity", b =>
                {
                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<string>("ChampionName")
                        .HasColumnType("TEXT");

                    b.Property<int>("Value")
                        .HasColumnType("INTEGER");

                    b.HasKey("Name");

                    b.HasIndex("ChampionName");

                    b.ToTable("Characteristic");

                    b.HasData(
                        new
                        {
                            Name = "Front-end",
                            Value = 100
                        },
                        new
                        {
                            Name = "Back-end",
                            Value = 100
                        });
                });

            modelBuilder.Entity("EntityFrameworkLOL.Entities.ImageEntity", b =>
                {
                    b.Property<string>("Base64")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.HasKey("Base64");

                    b.ToTable("Image");

                    b.HasData(
                        new
                        {
                            Base64 = "default"
                        });
                });

            modelBuilder.Entity("EntityFrameworkLOL.Entities.RuneEntity", b =>
                {
                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<int>("Family")
                        .HasColumnType("INTEGER");

                    b.Property<string>("ImageBase64")
                        .HasColumnType("TEXT");

                    b.HasKey("Name");

                    b.HasIndex("ImageBase64");

                    b.ToTable("Rune");

                    b.HasData(
                        new
                        {
                            Name = "Mastering of Blue",
                            Description = "Blue shades",
                            Family = 2
                        },
                        new
                        {
                            Name = "Empty Shards",
                            Description = "Remove runes",
                            Family = 1
                        });
                });

            modelBuilder.Entity("EntityFrameworkLOL.Entities.RunePageEntity", b =>
                {
                    b.Property<string>("Name")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.HasKey("Name");

                    b.ToTable("RunePage");

                    b.HasData(
                        new
                        {
                            Name = "FirstRunepage"
                        });
                });

            modelBuilder.Entity("EntityFrameworkLOL.Entities.SkillEntity", b =>
                {
                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<int>("Type")
                        .HasColumnType("INTEGER");

                    b.HasKey("Name");

                    b.ToTable("Skill");

                    b.HasData(
                        new
                        {
                            Name = "Beautiful layout",
                            Description = "Bowl'In",
                            Type = 3
                        },
                        new
                        {
                            Name = "DB Support",
                            Description = "DB",
                            Type = 1
                        });
                });

            modelBuilder.Entity("EntityFrameworkLOL.Entities.SkinEntity", b =>
                {
                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<string>("ChampionSkinName")
                        .HasColumnType("TEXT");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("Icon")
                        .HasColumnType("TEXT");

                    b.Property<string>("ImageBase64")
                        .HasColumnType("TEXT");

                    b.Property<float>("Price")
                        .HasColumnType("REAL");

                    b.HasKey("Name");

                    b.HasIndex("ChampionSkinName");

                    b.HasIndex("ImageBase64");

                    b.ToTable("Skin");

                    b.HasData(
                        new
                        {
                            Name = "Darker WinKer",
                            Description = "Be invisible in the darkness but never alone",
                            Icon = "default",
                            Price = 9.99f
                        },
                        new
                        {
                            Name = "Summer Daffodil",
                            Description = "A jewel of Summer for all year long",
                            Icon = "default",
                            Price = 9.99f
                        });
                });

            modelBuilder.Entity("RuneEntityRunePageEntity", b =>
                {
                    b.Property<string>("RunePagesName")
                        .HasColumnType("TEXT");

                    b.Property<string>("RunesName")
                        .HasColumnType("TEXT");

                    b.HasKey("RunePagesName", "RunesName");

                    b.HasIndex("RunesName");

                    b.ToTable("RuneEntityRunePageEntity");
                });

            modelBuilder.Entity("ChampionEntityRunePageEntity", b =>
                {
                    b.HasOne("EntityFrameworkLOL.Entities.ChampionEntity", null)
                        .WithMany()
                        .HasForeignKey("ChampionsName")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("EntityFrameworkLOL.Entities.RunePageEntity", null)
                        .WithMany()
                        .HasForeignKey("RunePagesName")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("ChampionEntitySkillEntity", b =>
                {
                    b.HasOne("EntityFrameworkLOL.Entities.ChampionEntity", null)
                        .WithMany()
                        .HasForeignKey("ChampionsName")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("EntityFrameworkLOL.Entities.SkillEntity", null)
                        .WithMany()
                        .HasForeignKey("SkillsName")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("EntityFrameworkLOL.Entities.ChampionEntity", b =>
                {
                    b.HasOne("EntityFrameworkLOL.Entities.ImageEntity", "Image")
                        .WithMany()
                        .HasForeignKey("ImageBase64");

                    b.Navigation("Image");
                });

            modelBuilder.Entity("EntityFrameworkLOL.Entities.CharacteristicEntity", b =>
                {
                    b.HasOne("EntityFrameworkLOL.Entities.ChampionEntity", "Champion")
                        .WithMany()
                        .HasForeignKey("ChampionName");

                    b.Navigation("Champion");
                });

            modelBuilder.Entity("EntityFrameworkLOL.Entities.RuneEntity", b =>
                {
                    b.HasOne("EntityFrameworkLOL.Entities.ImageEntity", "Image")
                        .WithMany()
                        .HasForeignKey("ImageBase64");

                    b.Navigation("Image");
                });

            modelBuilder.Entity("EntityFrameworkLOL.Entities.SkinEntity", b =>
                {
                    b.HasOne("EntityFrameworkLOL.Entities.ChampionEntity", "ChampionSkin")
                        .WithMany()
                        .HasForeignKey("ChampionSkinName");

                    b.HasOne("EntityFrameworkLOL.Entities.ImageEntity", "Image")
                        .WithMany()
                        .HasForeignKey("ImageBase64");

                    b.Navigation("ChampionSkin");

                    b.Navigation("Image");
                });

            modelBuilder.Entity("RuneEntityRunePageEntity", b =>
                {
                    b.HasOne("EntityFrameworkLOL.Entities.RunePageEntity", null)
                        .WithMany()
                        .HasForeignKey("RunePagesName")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("EntityFrameworkLOL.Entities.RuneEntity", null)
                        .WithMany()
                        .HasForeignKey("RunesName")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
