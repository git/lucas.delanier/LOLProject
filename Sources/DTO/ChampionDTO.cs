﻿using Model;
using System.Collections.Immutable;
using System.Collections.ObjectModel;

namespace DTO
{
    public class ChampionDTO
    {
        public string Name { get; set; }

        public string Bio { get; set; }

        public ChampionClass Class { get; set; }

        public string Icon { get; set; }

        public LargeImage Image { get; set; }


        public IEnumerable<SkillDTO> Skills { get; set; }
    }
}