﻿using APILOL.Mapper;
using Microsoft.EntityFrameworkCore;
using Model;
using System.Data.SqlTypes;
using System.Xml.Linq;

namespace ManagersEF
{
    public partial class EFManager
    {
        public class SkinsManager : ISkinsManager
        {
            private readonly EFManager parent;

            public SkinsManager(EFManager parent)
                => this.parent = parent;

            public async Task<Skin?> AddItem(Skin? item)
            {
                await parent.DbContext.Skin.AddAsync(item.ToEntity(parent.DbContext));
                parent.DbContext.SaveChanges();
                return item;
            }

            public async Task<bool> DeleteItem(Skin? item)
            {
                var toDelete = parent.DbContext.Skin.Find(item.Name);
                if (toDelete != null) 
                {
                    parent.DbContext.Skin.Remove(toDelete);
                    parent.DbContext.SaveChanges();
                    return true;
                }
                return false;
            }

            public async Task<IEnumerable<Skin?>> GetItems(int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.Skin.Include("Champion").GetItemsWithFilterAndOrdering(
                    s => true,
                    index, count,
                    orderingPropertyName, descending).Select(s => s?.ToModel());
            }

            public async Task<IEnumerable<Skin?>> GetItemsByChampion(Champion? champion, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.Skin.Include("Champion").GetItemsWithFilterAndOrdering(
                    s => s.ChampionSkin.Name.Equals(champion?.Name),
                    index, count,
                    orderingPropertyName, descending).Select(s => s?.ToModel());
            }

            public async Task<IEnumerable<Skin?>> GetItemsByName(string substring, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return parent.DbContext.Skin.Include("Champion").GetItemsWithFilterAndOrdering(
                    s => s.Name.Contains(substring),
                    index, count,
                    orderingPropertyName, descending).Select(s => s?.ToModel());
            }

            public async Task<int> GetNbItems()
            {
                return parent.DbContext.Skin.Count();
            }

            public async Task<int> GetNbItemsByChampion(Champion? champion)
            {
                if (champion != null)
                {
                    return parent.DbContext.Skin.Where(s => s.ChampionSkin.Name.Equals(champion.Name))
                    .Count();
                }
                return 0;
            }

            public async Task<int> GetNbItemsByName(string substring)
            {
                return parent.DbContext.Skin.Where(s => s.Name.Contains(substring))
                .Count();
            }

            public async Task<Skin?> UpdateItem(Skin? oldItem, Skin? newItem)
            {
                var toUpdate = parent.DbContext.Skin.Find(oldItem.Name);
                toUpdate.ChampionSkin = parent.DbContext.Champion.Find(newItem.Champion.Name);
                return newItem;
            }
        }
    }
}